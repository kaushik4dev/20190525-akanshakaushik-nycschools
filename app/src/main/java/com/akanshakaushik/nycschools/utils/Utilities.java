package com.akanshakaushik.nycschools.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.akanshakaushik.nycschools.service.model.School;

import java.util.Collections;
import java.util.List;

/**
 * Common Class to keep all Utility methods
 */
public class Utilities {

    /**
     * Ths method (function) for checking internet connection.
     *
     * @param context of the app.
     * @return true if internet connection is present else return false
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Ths method formats the address
     * removes latitude and longitude
     *
     * @param address is the address coming back from API with Latitude and Longitude
     * @return formatted address
     */
    public static String formatAddress(String address) {
        if (address != null && !address.isEmpty()) {
            int firstIndex = address.indexOf("(");
            if (firstIndex != -1) {
                address = address.substring(0, address.indexOf("("));
            }
        }
        return address;
    }

    /**
     * Ths method sorts the list alphabetically
     * Uses Collection API
     *
     * @param schoolsList is the unsorted schools list from API
     * @return sorted schools list
     */
    public static List<School> sortSchoolsListByName(List<School> schoolsList) {
        if (schoolsList != null) {
            Collections.sort(schoolsList, (schoolOne, schoolTwo) -> schoolOne.getName().compareTo(schoolTwo.getName()));
        }
        return schoolsList;
    }
}
