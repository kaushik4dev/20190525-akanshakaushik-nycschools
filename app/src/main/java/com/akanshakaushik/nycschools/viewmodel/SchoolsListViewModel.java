package com.akanshakaushik.nycschools.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.akanshakaushik.nycschools.service.model.School;
import com.akanshakaushik.nycschools.service.repository.SchoolsListRepository;

import java.util.List;

/**
 * ViewModel class for SchoolsListFragment
 */
public class SchoolsListViewModel extends ViewModel {

    private MutableLiveData<List<School>> schoolsList;
    SchoolsListRepository schoolsListRepository = SchoolsListRepository.getInstance();

    /**
     * Make api calls to retrieve schools list if not present in live data.
     */
    public void getSchoolsList() {
        if (schoolsList != null) {
            return;
        }
        schoolsList = schoolsListRepository.getSchoolsList();
    }

    /**
     * Expose the LiveData Schools List so UI can observe it.
     */
    public MutableLiveData<List<School>> getSchoolsListRepository(boolean refresh) {
        if (refresh) {
            return schoolsListRepository.getSchoolsList();
        }
        return schoolsList;
    }

}
