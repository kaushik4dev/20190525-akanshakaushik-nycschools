package com.akanshakaushik.nycschools.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.akanshakaushik.nycschools.service.model.ScoreDetails;
import com.akanshakaushik.nycschools.service.repository.ScoreDetailsRepository;

import java.util.List;

/**
 * ViewModel class for ScoreDetailsFragment
 */
public class ScoreDetailsViewModel extends ViewModel {

    private MutableLiveData<List<ScoreDetails>> scoreDetails;

    /**
     * Make api calls to retrieve score details if not present in live data.
     */
    public void getScoreDetails(String id) {
        if (scoreDetails != null) {
            return;
        }
        ScoreDetailsRepository scoreDetailsRepository = getRepository(id);
        scoreDetails = scoreDetailsRepository.getScoreDetails();
    }

    /**
     * Expose the LiveData Score details so UI can observe it.
     * @return LiveData score details
     */
    public MutableLiveData<List<ScoreDetails>> getScoreDetailsRepository() {
        return scoreDetails;
    }

    public ScoreDetailsRepository getRepository(String id) {
        return new ScoreDetailsRepository(id);
    }
}
