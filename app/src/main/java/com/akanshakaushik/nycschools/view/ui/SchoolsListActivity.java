package com.akanshakaushik.nycschools.view.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.akanshakaushik.nycschools.R;

/**
 * Activity to hold Schools list and Score card details
 */
public class SchoolsListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools_list);

        /*
          Add fragment to container
          checks savedInstanceState to avoid duplicate onCreateView calls on screen rotation
         */
        if (savedInstanceState == null) {
            SchoolsListFragment schoolsListFragment = new SchoolsListFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, schoolsListFragment)
                    .setReorderingAllowed(true)
                    .commit();
        }
    }
}
