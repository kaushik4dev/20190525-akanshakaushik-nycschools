package com.akanshakaushik.nycschools.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akanshakaushik.nycschools.R;
import com.akanshakaushik.nycschools.service.model.School;
import com.akanshakaushik.nycschools.utils.Utilities;
import com.akanshakaushik.nycschools.view.adapter.SchoolsListAdapter;
import com.akanshakaushik.nycschools.viewmodel.SchoolsListViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment class for Schools List
 * Contains RecyclerView which inflates list of schools
 */
public class SchoolsListFragment extends Fragment {

    View view;
    SchoolsListViewModel schoolsListViewModel;

    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.list_loading_progress_bar)
    ContentLoadingProgressBar loadingSpinner;

    @BindView(R.id.text_view_no_data)
    TextView textViewNoData;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.schools_list, container, false);
        ButterKnife.bind(this, view);

        schoolsListViewModel = ViewModelProviders.of(this).get(SchoolsListViewModel.class);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(true);

        if (Utilities.isConnected(getActivity())) {
            fetchSchoolsList(false);
        } else {
            updateViewNoDataAvailable();
        }

        /*
          Implementation of Pull down to refresh
          Fetch list from ViewModel on refreshing
         */
        swipeContainer.setOnRefreshListener(() -> {
            if (Utilities.isConnected(getActivity())) {
                fetchSchoolsList(true);
            } else {
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return view;
    }

    /**
     * Method to show progress bar while fetching the list from ViewModel
     */
    public void showProgress() {
        loadingSpinner.show();
        loadingSpinner.setVisibility(View.VISIBLE);
    }

    /**
     * Method to fetch the list from ViewModel
     * observe Live data object
     */
    public void fetchSchoolsList(boolean refresh) {
        showProgress();
        schoolsListViewModel.getSchoolsList();
        schoolsListViewModel.getSchoolsListRepository(refresh).observe(getViewLifecycleOwner(), schoolsResponse -> {
            SchoolsListFragment.this.dismissProgress();
            swipeContainer.setRefreshing(false);
            if (schoolsResponse != null) {
                SchoolsListFragment.this.setupRecyclerView(schoolsResponse);
            }
        });
    }

    /**
     * Method to dismiss progress bar after completing fetching of the list from ViewModel
     */
    public void dismissProgress() {
        loadingSpinner.hide();
        loadingSpinner.setVisibility(View.GONE);
    }

    /**
     * Method to set the list in recycler view
     */
    private void setupRecyclerView(List<School> schoolList) {
        SchoolsListAdapter schoolsListAdapter = new SchoolsListAdapter(getActivity(), schoolList);
        recyclerView.setAdapter(schoolsListAdapter);
    }

    /**
     * Method to show no data available view
     */
    public void updateViewNoDataAvailable() {
        loadingSpinner.setVisibility(View.GONE);
        textViewNoData.setVisibility(View.VISIBLE);
        Toast.makeText(getActivity(), getResources().getString(R.string.no_data) +
                getResources().getString(R.string.single_space) + getResources().getString(R.string.no_data_1), Toast.LENGTH_LONG).show();
    }
}
