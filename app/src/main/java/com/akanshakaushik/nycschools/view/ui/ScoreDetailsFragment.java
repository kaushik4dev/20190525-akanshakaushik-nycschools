package com.akanshakaushik.nycschools.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akanshakaushik.nycschools.R;
import com.akanshakaushik.nycschools.constants.Constants;
import com.akanshakaushik.nycschools.service.model.ScoreDetails;
import com.akanshakaushik.nycschools.utils.Utilities;
import com.akanshakaushik.nycschools.viewmodel.ScoreDetailsViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment class to show SAT Score Details
 */
public class ScoreDetailsFragment extends Fragment {

    View view;
    ScoreDetailsViewModel scoreDetailsViewModel;

    @BindView(R.id.list_loading_progress_bar)
    ContentLoadingProgressBar loadingSpinner;

    @BindView(R.id.text_view_school_name)
    TextView textViewSchoolName;

    @BindView(R.id.text_view_total_test_takers)
    TextView textViewTotalTestTakers;

    @BindView(R.id.text_view_writing_score)
    TextView textViewWritingScore;

    @BindView(R.id.text_view_reading_score)
    TextView textViewReadingScore;

    @BindView(R.id.text_view_math_score)
    TextView textViewMathScore;

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_score_details, container, false);
        ButterKnife.bind(this, view);
        Bundle arguments = getArguments();
        String schoolId = arguments != null ? arguments.getString(Constants.Bundle.SCHOOL_ID_KEY) : null;

        scoreDetailsViewModel = ViewModelProviders.of(this).get(ScoreDetailsViewModel.class);

        // If there is a network connection, fetch data
        if (Utilities.isConnected(getActivity())) {
            showProgress();
            scoreDetailsViewModel.getScoreDetails(schoolId);
            scoreDetailsViewModel.getScoreDetailsRepository().observe(getViewLifecycleOwner(), scoreResponse -> {
                dismissProgress();
                updateView(scoreResponse);
            });
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_data) +
                    getResources().getString(R.string.single_space) + getResources().getString(R.string.no_data_1), Toast.LENGTH_SHORT).show();
            textViewSchoolName.setText(getResources().getString(R.string.no_data_1));
        }
        return view;
    }

    /**
     * Method to show progress bar while fetching score card details from View Model
     */
    public void showProgress() {
        loadingSpinner.show();
        loadingSpinner.setVisibility(View.VISIBLE);
    }

    /**
     * Method to dismiss progress bar after completing fetching of score card details from ViewModel
     */
    public void dismissProgress() {
        loadingSpinner.hide();
        loadingSpinner.setVisibility(View.GONE);
    }

    /**
     * Method to update the view after getting score card
     */
    public void updateView(List<ScoreDetails> scoreDetailsList) {
        if (scoreDetailsList != null && !scoreDetailsList.isEmpty()) {
            ScoreDetails scoreDetails = scoreDetailsList.get(0);
            textViewSchoolName.setText(scoreDetails.getSchoolName());
            textViewTotalTestTakers.setText(getResources().getString(R.string.total_test_takers, scoreDetails.getTotalTestTakers()));
            textViewWritingScore.setText(getResources().getString(R.string.average_writing_score, scoreDetails.getWritingAverageScore()));
            textViewReadingScore.setText(getResources().getString(R.string.average_reading_scores, scoreDetails.getCriticalReadingScore()));
            textViewMathScore.setText(getResources().getString(R.string.average_math_score, scoreDetails.getMathAverageScore()));
        } else {
            // Show no data available view if score card data is not available
            Toast.makeText(getActivity(), getResources().getString(R.string.no_data) +
                    getResources().getString(R.string.single_space) + getResources().getString(R.string.no_data_1), Toast.LENGTH_SHORT).show();
            textViewSchoolName.setText(getResources().getString(R.string.no_data_1));
        }
    }
}
