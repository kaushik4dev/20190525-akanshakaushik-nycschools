package com.akanshakaushik.nycschools.service.repository;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.akanshakaushik.nycschools.service.model.School;
import com.akanshakaushik.nycschools.utils.Utilities;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Repository class to retrieve List of Schools
 * Uses MutableLiveData to send data to ViewModel
 */
public class SchoolsListRepository {

    private static SchoolsListRepository schoolsListRepository;
    private static final String TAG = SchoolsListRepository.class.getSimpleName();

    /**
     * Singleton Instance to access SchoolsListRepository
     */
    public static SchoolsListRepository getInstance() {
        if (schoolsListRepository == null) {
            schoolsListRepository = new SchoolsListRepository();
        }
        return schoolsListRepository;
    }

    /**
     * Get List of Schools from API, then set it in LiveData
     * @return MutableLiveData
     */
    public MutableLiveData<List<School>> getSchoolsList() {
        final MutableLiveData<List<School>> data = new MutableLiveData<>();
        ApiNetworkClient.Companion.getNetworkClient().getSchoolsList().clone().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(@NonNull Call<List<School>> call, @NonNull Response<List<School>> response) {
                if (response.isSuccessful()) {
                    data.setValue(Utilities.sortSchoolsListByName(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<School>> call, @NonNull Throwable t) {
                Log.e(TAG, "Error in Retrieving School List");
                data.setValue(null);
            }
        });

        return data;
    }
}
