package com.akanshakaushik.nycschools.service.repository

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

import com.akanshakaushik.nycschools.constants.Constants.Service.PATH_SCHOOL_LISTS
import com.akanshakaushik.nycschools.constants.Constants.Service.PATH_SCORE_DETAILS
import com.akanshakaushik.nycschools.constants.Constants.Service.QUERY_DBN
import com.akanshakaushik.nycschools.service.model.School
import com.akanshakaushik.nycschools.service.model.ScoreDetails

/**
 * Retrofit Interface to communicate with service Request & Response
 */
interface IApiNetworkClient {

    @get:GET(PATH_SCHOOL_LISTS)
    val schoolsList: Call<List<School>>

    @GET(PATH_SCORE_DETAILS)
    fun getScoreDetails(@Query(QUERY_DBN) dbn: String): Call<List<ScoreDetails>>
}
