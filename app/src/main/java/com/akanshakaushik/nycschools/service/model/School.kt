package com.akanshakaushik.nycschools.service.model

import android.os.Parcel
import android.os.Parcelable

import com.akanshakaushik.nycschools.utils.Utilities
import com.google.gson.annotations.SerializedName

import com.akanshakaushik.nycschools.constants.Constants.Service.TAG_DBN
import com.akanshakaushik.nycschools.constants.Constants.Service.TAG_LOCATION
import com.akanshakaushik.nycschools.constants.Constants.Service.TAG_PHONE_NUMBER
import com.akanshakaushik.nycschools.constants.Constants.Service.TAG_SCHOOL_NAME

/**
 * Model class for School
 * Used Parcelable for efficiency
 */
class School : Parcelable {

    @SerializedName(TAG_DBN)
    var id: String? = null

    @SerializedName(TAG_SCHOOL_NAME)
    var name: String? = null

    @SerializedName(TAG_PHONE_NUMBER)
    var phoneNumber: String? = null

    @SerializedName(TAG_LOCATION)
    private var address: String? = null

    constructor(parcel: Parcel) {
        id = parcel.readString()
        name = parcel.readString()
        phoneNumber = parcel.readString()
        address = parcel.readString()
    }

    constructor(name: String) {
        this.name = name
    }

    constructor() {}

    fun getAddress(): String? {
        // Formatting address to remove Latitude and Longitude
        return Utilities.formatAddress(address)
    }

    fun setAddress(address: String) {
        this.address = address
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(name)
        dest.writeString(address)
        dest.writeString(phoneNumber)
    }

    companion object {

        @JvmField
        val CREATOR: Parcelable.Creator<School> = object : Parcelable.Creator<School> {
            override fun createFromParcel(source: Parcel): School {
                return School(source)
            }

            override fun newArray(size: Int): Array<School?> {
                return arrayOfNulls(size)
            }
        }
    }
}
