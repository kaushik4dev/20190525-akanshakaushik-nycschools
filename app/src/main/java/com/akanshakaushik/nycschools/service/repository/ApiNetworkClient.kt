package com.akanshakaushik.nycschools.service.repository

import com.akanshakaushik.nycschools.BuildConfig
import com.akanshakaushik.nycschools.constants.Constants.Service.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * ApiNetworkClient class is main Retrofit Api Client with Singleton instance
 */
class ApiNetworkClient
/**
 * Private constructor to restrict the instantiation of class.
 */
private constructor() {

    init {
        throw AssertionError()
    }

    companion object {

        private var retrofit: Retrofit.Builder? = null

        /**
         * Method to get the network client interface.
         *
         * @return - return network client.
         */
        val networkClient: IApiNetworkClient
            get() {
                if (retrofit == null) {
                    retrofit = Retrofit.Builder()
                    val httpClient = OkHttpClient.Builder()
                    httpClient.readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS)
                    if (BuildConfig.DEBUG) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                        httpClient.addInterceptor(loggingInterceptor)
                    }
                    retrofit!!.client(httpClient.build())
                    retrofit!!.addConverterFactory(GsonConverterFactory.create())
                }
                retrofit!!.baseUrl(BASE_URL)
                return retrofit!!.build().create(IApiNetworkClient::class.java)
            }
    }
}
