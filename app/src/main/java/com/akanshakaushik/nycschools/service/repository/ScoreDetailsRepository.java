package com.akanshakaushik.nycschools.service.repository;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.akanshakaushik.nycschools.service.model.ScoreDetails;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Repository class to retrieve SAT Score card details
 * Uses MutableLiveData to send data to ViewModel
 */
public class ScoreDetailsRepository {

    private static final String TAG = ScoreDetailsRepository.class.getSimpleName();
    private Call<List<ScoreDetails>> scoreDetailsServiceCall;

    /**
     * Constructor to access ScoreDetailsRepository
     *
     * @param id is school Id passed when a particular school is selected
     */
    public ScoreDetailsRepository(String id) {
        this.scoreDetailsServiceCall = ApiNetworkClient.Companion.getNetworkClient().getScoreDetails(id);
    }

    /**
     * Get Score card details from API, then set it in LiveData
     *
     * @return MutableLiveData
     */
    public MutableLiveData<List<ScoreDetails>> getScoreDetails() {
        final MutableLiveData<List<ScoreDetails>> data = new MutableLiveData<>();
        scoreDetailsServiceCall.enqueue(new Callback<List<ScoreDetails>>() {
            @Override
            public void onResponse(@NotNull Call<List<ScoreDetails>> call, @NotNull Response<List<ScoreDetails>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NotNull Call<List<ScoreDetails>> call, @NotNull Throwable t) {
                Log.e(TAG, "Error in Retrieving Score Details");
                data.setValue(null);
            }
        });

        return data;
    }
}
