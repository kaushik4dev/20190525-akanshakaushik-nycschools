package com.akanshakaushik.nycschools.service.model

import android.os.Parcel
import android.os.Parcelable
import com.akanshakaushik.nycschools.constants.Constants.Service.*
import com.google.gson.annotations.SerializedName

/**
 * Model class for SAT Score details
 * Used Parcelable for efficiency
 */
class ScoreDetails(parcel: Parcel) : Parcelable {

    @SerializedName(TAG_DBN)
    var id: String? = null

    @SerializedName(TAG_NO_OF_TEST_TAKERS)
    var totalTestTakers: String? = null

    @SerializedName(TAG_AVERAGE_READING_SCORE)
    var criticalReadingScore: String? = null

    @SerializedName(TAG_AVERAGE_MATH_SCORE)
    var mathAverageScore: String? = null

    @SerializedName(TAG_AVERAGE_WRITING_SCORE)
    var writingAverageScore: String? = null

    @SerializedName(TAG_SCHOOL_NAME)
    var schoolName: String? = null


    init {
        id = parcel.readString()
        totalTestTakers = parcel.readString()
        criticalReadingScore = parcel.readString()
        mathAverageScore = parcel.readString()
        writingAverageScore = parcel.readString()
        schoolName = parcel.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(totalTestTakers)
        dest.writeString(criticalReadingScore)
        dest.writeString(mathAverageScore)
        dest.writeString(writingAverageScore)
        dest.writeString(schoolName)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ScoreDetails> = object : Parcelable.Creator<ScoreDetails> {
            override fun createFromParcel(source: Parcel): ScoreDetails {
                return ScoreDetails(source)
            }

            override fun newArray(size: Int): Array<ScoreDetails?> {
                return arrayOfNulls(size)
            }
        }
    }
}
