package com.akanshakaushik.nycschools.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UtilitiesTest {

    @Mock
    private Context context;

    @Test
    public void testFormatAddress() {
        String input = "Test(Value)";
        String expected = "Test";
        assertEquals(expected, Utilities.formatAddress(input));
    }

    @Test
    public void testFormatAddressForDifferentCharacters() {
        String input = "Test{Value)";
        String expected = "Test{Value)";
        assertEquals(expected, Utilities.formatAddress(input));
    }

    @Test
    public void testFormatAddressForEmptyAddress() {
        String input = "";
        String expected = "";
        assertEquals(expected, Utilities.formatAddress(input));
    }

    @Test
    public void testFormatAddressForNullAddress() {
        String input = null;
        String expected = null;
        assertEquals(expected, Utilities.formatAddress(input));
    }

    @Test
    public void hasActiveConnection() {

        ConnectivityManager mockConnectivityManager = mock(ConnectivityManager.class);
        NetworkInfo mockNetworkInfo = mock(NetworkInfo.class);
        when(context.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager);
        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(mockNetworkInfo);

        doAnswer(invocation -> true).when(mockNetworkInfo).isConnected();

        boolean hasActiveConnection = Utilities.isConnected(context);
        assertTrue(hasActiveConnection);
    }

    @Test
    public void hasNoActiveConnection() {

        ConnectivityManager mockConnectivityManager = mock(ConnectivityManager.class);
        NetworkInfo mockNetworkInfo = mock(NetworkInfo.class);
        when(context.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager);
        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(mockNetworkInfo);

        doAnswer(invocation -> false).when(mockNetworkInfo).isConnected();

        boolean hasActiveConnection = Utilities.isConnected(context);
        assertFalse(hasActiveConnection);
    }
}