package com.akanshakaushik.nycschools.utils;

import com.akanshakaushik.nycschools.service.model.School;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ListSortingTest {

    private List<School> sortedSchoolsList;
    private List<School> unsortedSchoolsList;

    @Before
    public void setUp() {
        School schoolOne = new School("Anthony School");
        School schoolTwo = new School("Baker School");
        School schoolThree = new School("Crater School");
        School schoolFour = new School("Flicker School");

        unsortedSchoolsList = Arrays.asList(
                schoolFour,
                schoolOne,
                schoolThree,
                schoolTwo
        );
    }

    @Test
    public void testListIsNotNullAndCorrectSize() {
        sortedSchoolsList = Utilities.sortSchoolsListByName(unsortedSchoolsList);
        assertNotNull(sortedSchoolsList);
        assertEquals(4, sortedSchoolsList.size());
    }

    @Test
    public void testSortingOrderIsCorrect() {
        sortedSchoolsList = Utilities.sortSchoolsListByName(unsortedSchoolsList);
        assertEquals("Anthony School", sortedSchoolsList.get(0).getName());
        assertEquals("Baker School", sortedSchoolsList.get(1).getName());
        assertEquals("Crater School", sortedSchoolsList.get(2).getName());
        assertEquals("Flicker School", sortedSchoolsList.get(3).getName());
    }

    @Test
    public void testSortingMethodReturnsNullWithNullInput() {
        sortedSchoolsList = Utilities.sortSchoolsListByName(null);
        assertNull(sortedSchoolsList);
    }

    @After
    public void tearDown() {
        unsortedSchoolsList = new ArrayList<>();
    }
}