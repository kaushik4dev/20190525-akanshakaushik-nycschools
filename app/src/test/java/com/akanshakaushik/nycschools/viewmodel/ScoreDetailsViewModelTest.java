package com.akanshakaushik.nycschools.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;

import com.akanshakaushik.nycschools.service.model.ScoreDetails;
import com.akanshakaushik.nycschools.service.repository.ScoreDetailsRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ScoreDetailsViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private ScoreDetailsViewModel scoreDetailsViewModel;

    @Mock
    private ScoreDetailsRepository scoreDetailsRepository;

    @Mock
    private Observer<List<ScoreDetails>> scoreDetailsObserver;

    @Mock
    private MutableLiveData<List<ScoreDetails>> scoreDetailsList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        scoreDetailsViewModel = Mockito.spy(new ScoreDetailsViewModel());
    }

    @Test
    public void testGetScoreDetails() throws Exception {
        doReturn(scoreDetailsRepository).when(scoreDetailsViewModel).getRepository(anyString());
        doReturn(scoreDetailsList).when(scoreDetailsRepository).getScoreDetails();
        scoreDetailsViewModel.getScoreDetails("ghdhdb");
        scoreDetailsViewModel.getScoreDetailsRepository().observeForever(scoreDetailsObserver);
        verify(scoreDetailsRepository).getScoreDetails();
        assertNotNull(scoreDetailsViewModel.getScoreDetailsRepository());
    }

    @Test
    public void testGetSchoolListCheckInvocations() {
        doReturn(scoreDetailsRepository).when(scoreDetailsViewModel).getRepository(anyString());
        doReturn(scoreDetailsList).when(scoreDetailsRepository).getScoreDetails();
        scoreDetailsViewModel.getScoreDetails("sbsbs");
        scoreDetailsViewModel.getScoreDetails("sbsbbs");
        verify(scoreDetailsRepository, times(1)).getScoreDetails();
        assertNotNull(scoreDetailsViewModel.getScoreDetailsRepository());
    }

    @After
    public void tearDown() {
        scoreDetailsViewModel = null;
    }
}