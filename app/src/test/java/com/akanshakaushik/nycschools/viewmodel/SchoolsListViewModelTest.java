package com.akanshakaushik.nycschools.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;

import com.akanshakaushik.nycschools.service.model.School;
import com.akanshakaushik.nycschools.service.repository.SchoolsListRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SchoolsListViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private SchoolsListViewModel schoolsListViewModel;

    @Mock
    private SchoolsListRepository schoolsListRepository;

    @Mock
    private Observer<List<School>> schoolsListObserver;

    @Mock
    private MutableLiveData<List<School>> schoolList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        schoolsListViewModel = new SchoolsListViewModel();
        schoolsListViewModel.schoolsListRepository = schoolsListRepository;
    }

    @Test
    public void testGetSchoolList() {
        doReturn(schoolList).when(schoolsListRepository).getSchoolsList();
        schoolsListViewModel.getSchoolsList();
        schoolsListViewModel.getSchoolsListRepository(anyBoolean()).observeForever(schoolsListObserver);
        verify(schoolsListRepository).getSchoolsList();
        assertNotNull(schoolsListViewModel.getSchoolsListRepository(false));
    }

    @Test
    public void testGetSchoolListCheckInvocations() {
        doReturn(schoolList).when(schoolsListRepository).getSchoolsList();
        schoolsListViewModel.getSchoolsList();
        schoolsListViewModel.getSchoolsList();
        verify(schoolsListRepository, times(1)).getSchoolsList();
        assertNotNull(schoolsListViewModel.getSchoolsListRepository(false));
    }

    @After
    public void tearDown() {
        schoolsListViewModel = null;
    }
}