# NYC Schools

An Android App which provides list of schools in Newyork City and their performance in SAT

## Table Of Contents
- [Features](#features)
- [Code Architecture](#code-architecture)
- [Specifications](#specifications)

## Features
The application provides the following features:
- List of schools in NewYork City
- List of schools **sorted alphabetically** so that it is easier to find the schools
- Address and phone number of the school in cardview. Navigate to the school by tapping on Address. Also, call in school office by tapping on phone number
- Tap on school item to view school's SAT performance
- SAT Score Detail Screen shows total test takers, average reading, writing and math score of the school in SAT.

## Code Architecture
Followed **Model-View-ViewModel** architecture. Below is the architecture diagram.

![mvvm-architecture-nyc-schools](https://user-images.githubusercontent.com/26492811/58387581-c10f8d80-7fde-11e9-8db7-33dbae10e795.png)

## Specifications

Minimum SDK Version: **LOLLIPOP**
Target SDK Version: **PIE**